using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyPlayer : MonoBehaviour
{
    [SerializeField]
    private int rings;

    [SerializeField]
    private int health;

    private int MAX_HEALTH = 100;

    public void Start()
    {
        rings = 0;
        health = MAX_HEALTH;
    }

    public void addRing()
    {
        rings++;
    }

    public void addHealth(int increment)
    {
        health += increment;

        if(health > MAX_HEALTH)
            health = MAX_HEALTH;
    }

}