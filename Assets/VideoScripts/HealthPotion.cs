using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// A health potion
public class HealthPotion : MyPickup
{
    [SerializeField]
    private int hpIncrease = 20;

    public override void collect(MyPlayer player)
    {
        player.addHealth(hpIncrease);
    }
}