using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Contract for pickups
// All pickups implement this. 

public interface IMyPickup
{
    // All childred will have
    void collect(MyPlayer player);
}

// Base class for pickups
// Other benefit - works in editor
// Abstract - can't make one 

public abstract class MyPickup : MonoBehaviour, IMyPickup
{
    // Delegate specifics
    public abstract void collect(MyPlayer player);

    // Common stuff

    private bool collected;

    public void Awake()
    {
        collected = false;
    }

    void OnTriggerEnter(Collider trigger)
    {
        // Am I collected
        if(collected == true) return;

        // Get player from collision
        GameObject go = trigger.gameObject;

        MyPlayer player = go.GetComponent<MyPlayer>();

        // if not player do nothing
        if(player == null)
            return;
        else
        {
            // if player call collect with player
            this.collect(player);
            collected = true;

            // turn off rendering fro the object. 
            MeshRenderer renderer = gameObject.GetComponent<MeshRenderer>();
            renderer.enabled = false;
        }

       
    }
}