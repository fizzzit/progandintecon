using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This defines the interface (abstract class) for Locks
public interface ILock
{
    // The derived class will use this 
    // instance of player to modify player
	bool tryLock(Player player);
}

// Let's first declare an Lock class
// this is a hack to get the children of Lock 
// displayed correctly in the editor
public abstract class Lock : MonoBehaviour, ILock
{
    public abstract bool tryLock(Player player);

    private bool open;

    public void Awake()
    {
        open = false;
    }

    void OnTriggerStay(Collider trigger)
    {
        //Debug.Log("Player at the door!");
        if(open) return;

        GameObject go = trigger.gameObject;
        
        Player player;
        player = go.GetComponent<Player>();

        if(player != null)
        {    
            open = this.tryLock(player);
        }
            
    }

}