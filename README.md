# README #

This is the repository for my internal economy and progression examples, the original version was based on the 3Cs repo.  I've kept the original sandbox and movement using the character controller.

**Note:** The plan was to start with progression mechanics.  I wanted to implement lock and key but decided the first thing I needed to do was pick up the key, so I created a pickup example with coins and health - i.e. internal economies!

## Scenes ##

These are the scenes included in the Unity3D project. 

### Sandbox ###
* Fixed camera
* Script to generate a tiled floor (from textured cube tiles,) and some steps.
### CC-FixedCam ###
* Fixed camera
* Floor / steps generation. 
* Player movement via the character controller
### Pickup ###
* Pickup interface and base class
* Specialised **Coin** class
* Specialised **Healthpack** class
* Player class, player state updated by the Pickup. 
### LockAndKey ###
* Specialised **Key**s (some types of keys)
* **Keyring** (holds the keys, keeps it out of **Player**). 
* **Door**s (look for a **Keyring** with the right **Key** to open). -- **NOT DONE**
### DynamicLock (open the door with rings?) ###
### Next level / zone (change scene, preserve objects - Player/Game) ###
